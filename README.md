Instructions
--------------------
- Download VirtualBox (https://www.virtualbox.org/wiki/Downloads).

- Install Vagrant (https://www.vagrantup.com/docs/installation).

- Create development directory.

	a. Windows: C:\develop\Vagrant\development 
	(Windows Command Line Interface [WCLI]: mkdir \develop\Vagrant\development)
	
	b. Linux: /develop/Vagrant/development  (Terminal [T]: mkdir /develop/Vagrant/development)

	
-- PULL DEV ENVIRONMENT DEPENDENCIES FROM BITBUCKET (GIT) --

- (Skip if you already have GIT installed). Install GIT 
(https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

- Change to the newly created 'develop/Vagrant/development' directory.

	a. [WCLI]: 'cd C:\develop\Vagrant\development' or [T]: 'cd /develop/Vagrant/development'

- Initialize directory and pull dependencies.

	a. [WCLI/T]:

		1. 'git init'

		2. 'git remote add origin 
		https://{yourbitbucketusername}@bitbucket.org/capitolarmorydevteam/ca-lamp-setup.git'

		3. 'git pull origin master' (Enter in credentials if necessary, username-email and your git 
		password)
		
	!If you have issues you can try cloning the repository which will make a folder; copy and 
	paste all those files to the development directory.


-- VAGRANT --

At this point you should have all the necessary files to get Vagrant to run with your 'www' directory already built. 

- Open Terminal and CD into the development directory. By now you should already know how to do this.

	a. Let’s build or Vagrant Box by [T]: 'vagrant up'
	
	!This may take a while so be patient (grab some coffee -_-). Vagrant is building your VM with 
	all the LAMP stack dependencies.