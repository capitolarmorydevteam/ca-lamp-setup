#!/bin/bash

# Update repository
sudo apt-get -y update

# Install Apache
sudo apt-get -y install apache2

# Check '/var/www' Symlink
if ! [ -L /var/www/html ]; then
	rm -rf /var/www/html
	ln -fs /vagrant /var/www/html
fi

# Install MySQL
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password nfatools'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password nfatools'
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql

# Install PHP and PHP Dependencies
sudo apt-get -y install php5 libapache2-mod-php5 php5-mcrypt

# Install Imagemagick
sudo apt-get install imagemagick

# Install Composer
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php -r "if (hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php
sudo php -r "unlink('composer-setup.php');"
sudo mv ~/composer.phar /usr/local/bin/composer	

# Install X-Debug
sudo apt-get install php5-xdebug
